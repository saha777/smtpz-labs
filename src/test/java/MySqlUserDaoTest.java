import com.lab.dao.DbType;
import com.lab.dao.UserDao;
import com.lab.dao.factory.UserDaoFactory;
import com.lab.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MySqlUserDaoTest {

    private static UserDao userDao;

    @Before
    public void beforeTest() {
        userDao = UserDaoFactory.create(DbType.MY_SQL);
    }

    @Test
    public void mySqlUserDaoTest() {
        User user = new User();
        user.setId(-1);
        user.setName("Test");
        user.setLastname("Test");
        user.setAge((byte) 22);

        userDao.insert(user);

        List<User> users = userDao.findAll();

        for (User u : users) {
            if (user.getName().equals(u.getName())) {
                user.setId(u.getId());
                break;
            }
        }

        Assert.assertNotEquals(user.getId(), -1);

        user.setName("Test2");
        userDao.update(user);
        User fromDB = userDao.findById(user.getId());

        Assert.assertNotNull(fromDB);
        Assert.assertEquals(user.getName(), fromDB.getName());

        userDao.delete(user.getId());

        fromDB = userDao.findById(user.getId());
        Assert.assertNull(fromDB);
    }


}
