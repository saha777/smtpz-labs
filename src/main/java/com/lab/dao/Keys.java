package com.lab.dao;

public class Keys {
    public static final String MY_SQL_DRIVER = "com.mysql.jdbc.Driver";
    public static final String MY_SQL_DB_URL = "jdbc:mysql://localhost:3306/demo_db";
    public static final String MY_SQL_DB_USER = "root";
    public static final String MY_SQL_DB_PASS = "root";

    public static final String MONGO_HOST = "localhost";
    public static final int MONGO_PORT = 27017;
    public static final String MONGO_DB_NAME ="UserDB";
}
