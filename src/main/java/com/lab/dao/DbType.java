package com.lab.dao;

public enum DbType {
    MY_SQL, MONGO_DB
}
