package com.lab.dao;

public class SqlQueries {
    public static final String USER_DAO_FIND_ALL = "SELECT id, name, lastname, age FROM demo_db.users";
    public static final String USER_DAO_FIND_BY_ID = "SELECT id, name, lastname, age FROM demo_db.users WHERE id = ?";
    public static final String USER_DAO_INSERT = "INSERT INTO demo_db.users(name, lastname, age) VALUES(?, ?, ?)";
    public static final String USER_DAO_UPDATE = "UPDATE demo_db.users SET name = ?, lastname = ?, age = ? WHERE id = ?";
    public static final String USER_DAO_DELETE = "DELETE FROM demo_db.users WHERE id = ?";

    private SqlQueries() {}
}
