package com.lab.dao.factory;

import com.lab.dao.DbType;
import com.lab.dao.UserDao;
import com.lab.dao.impls.MongoDBUserDao;
import com.lab.dao.impls.MySqlUserDao;

public class UserDaoFactory {

    public static UserDao create(DbType dbType) {
        if (DbType.MY_SQL.equals(dbType)) {
            return new MySqlUserDao();
        } else {
            return new MongoDBUserDao();
        }
    }
}
