package com.lab.dao.impls;

import com.lab.dao.SqlQueries;
import com.lab.dao.UserDao;
import com.lab.dao.exception.SQLRuntimeException;
import com.lab.dao.mapper.Mapper;
import com.lab.dao.mapper.UserMapper;
import com.lab.dao.template.JDBCTemplates;
import com.lab.dao.template.impls.JDBCTemplatesImpl;
import com.lab.model.User;

import java.sql.*;
import java.util.List;

public class MySqlUserDao implements UserDao {

    private Mapper<User, ResultSet> userMapper;
    private JDBCTemplates<User, ResultSet> jdbcTemplates;

    public MySqlUserDao() {
        userMapper = new UserMapperImpl();
        jdbcTemplates = new JDBCTemplatesImpl<>();
    }

    public List<User> findAll() {
        List<User> users;
        try {
            users = jdbcTemplates.find(SqlQueries.USER_DAO_FIND_ALL, userMapper);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLRuntimeException();
        }
        return users;
    }

    public User findById(int id) {
        User user;
        try {
            user = jdbcTemplates.findOne(SqlQueries.USER_DAO_FIND_BY_ID, userMapper, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLRuntimeException();
        }
        return user;
    }

    public void insert(User user) {
        try {
            jdbcTemplates.update(SqlQueries.USER_DAO_INSERT,
                    user.getName(), user.getLastname(), user.getAge());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLRuntimeException();
        }
    }

    public void update(User user) {
        try {
            jdbcTemplates.update(SqlQueries.USER_DAO_UPDATE,
                    user.getName(), user.getLastname(), user.getAge(), user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLRuntimeException();
        }
    }

    public void delete(int id) {
        try {
            jdbcTemplates.update(SqlQueries.USER_DAO_DELETE, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLRuntimeException();
        }
    }

    private class UserMapperImpl implements UserMapper<ResultSet> {
        public User map(ResultSet resultSet) {
            User user = new User();
            try {
                user.setId(resultSet.getInt(ID_FIELD));
                user.setName(resultSet.getString(NAME_FIELD));
                user.setLastname(resultSet.getString(LASTNAME_FIELD));
                user.setAge(resultSet.getByte(AGE_FIELD));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return user;
        }
    }
}
