package com.lab.dao.impls;

import com.mongodb.*;
import com.lab.dao.Keys;
import com.lab.dao.UserDao;
import com.lab.dao.mapper.Mapper;
import com.lab.dao.mapper.UserMapper;
import com.lab.model.User;

import java.util.ArrayList;
import java.util.List;

public class MongoDBUserDao implements UserDao {
    private static final String COLLECTION_NAME = "users";
    private static final String DOT = "\\.";

    private Mapper<User, DBObject> mapper;
    private DB db;
    private DBCollection collection;

    public MongoDBUserDao() {
        mapper = new UserMapperImpl();
        MongoClient mongoClient = new MongoClient(Keys.MONGO_HOST, Keys.MONGO_PORT);
        db = mongoClient.getDB(Keys.MONGO_DB_NAME);
        collection = db.getCollection(COLLECTION_NAME);
    }

    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        DBCursor cursor = collection.find();

        while (cursor.hasNext()) {
            users.add(mapper.map(cursor.next()));
        }

        return users;
    }

    public User findById(int id) {
        User user = null;

        BasicDBObject searchQuery = new BasicDBObject(UserMapperImpl.ID_FIELD, id);

        DBCursor cursor = collection.find(searchQuery);

        if (cursor.hasNext()) {
            user = mapper.map(cursor.next());
        }

        return user;
    }

    public void insert(User user) {
        final String AI_FUNC = "getNextSequenceValue('userid')";
        final String RETVAL = "retval";

        user.setId(Integer.parseInt(
                db.doEval(AI_FUNC)
                .get(RETVAL)
                .toString()
                .split(DOT)[0]));

        BasicDBObject document = new BasicDBObject();

        document.put(UserMapperImpl.ID_FIELD, user.getId());
        document.put(UserMapper.NAME_FIELD, user.getName());
        document.put(UserMapper.LASTNAME_FIELD, user.getLastname());
        document.put(UserMapper.AGE_FIELD, user.getAge());

        collection.insert(document);
    }

    public void update(User user) {
        final String SET = "$set";

        BasicDBObject updateUser = new BasicDBObject();
        updateUser.put(UserMapper.NAME_FIELD, user.getName());
        updateUser.put(UserMapper.LASTNAME_FIELD, user.getLastname());
        updateUser.put(UserMapper.AGE_FIELD, user.getAge());

        final BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.append(SET, updateUser);

        final BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put(UserMapperImpl.ID_FIELD, user.getId());

        collection.update(searchQuery, updateQuery);
    }

    public void delete(int id) {
        collection.remove(new BasicDBObject(UserMapperImpl.ID_FIELD, id));
    }

    private class UserMapperImpl implements UserMapper<DBObject> {
        private static final String ID_FIELD = "_id";

        public User map(DBObject cursor) {
            User user = new User();

            user.setId(Integer.parseInt(cursor.get(ID_FIELD).toString()));
            user.setName(cursor.get(NAME_FIELD).toString());
            user.setLastname(cursor.get(LASTNAME_FIELD).toString());
            user.setAge(Byte.parseByte(cursor.get(AGE_FIELD).toString()));

            return user;
        }
    }
}
