package com.lab.dao.template;

import com.lab.dao.mapper.Mapper;

import java.sql.SQLException;
import java.util.List;

public interface JDBCTemplates<T, D> {
    List<T> find(String query, Mapper<T, D> mapper) throws SQLException;

    List<T> find(String query, Mapper<T, D> mapper, Object ... params) throws SQLException;

    T findOne(String query, Mapper<T, D> mapper, Object ... params) throws SQLException;

    void update(String query, Object ... params) throws SQLException;
}
