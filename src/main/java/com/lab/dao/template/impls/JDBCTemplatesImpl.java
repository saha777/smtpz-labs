package com.lab.dao.template.impls;

import com.lab.dao.Keys;
import com.lab.dao.mapper.Mapper;
import com.lab.dao.template.JDBCTemplates;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCTemplatesImpl<T> implements JDBCTemplates<T, ResultSet> {

    public List<T> find(String query, Mapper<T, ResultSet> mapper) throws SQLException {
        Connection connection = null;
        List<T> result = new ArrayList<T>();
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(mapper.map(resultSet));
            }
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<T> find(String query, Mapper<T, ResultSet> mapper, Object ... params) throws SQLException {
        Connection connection = null;
        List<T> result = new ArrayList<T>();
        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result.add(mapper.map(resultSet));
            }
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public T findOne(String query, Mapper<T, ResultSet> mapper, Object ... params) throws SQLException {
        Connection connection = null;
        T temp = null;
        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                temp = mapper.map(resultSet);
            }
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return temp;
    }

    public void update(String query, Object ... params) throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }
            preparedStatement.execute();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected Connection getConnection() throws SQLException {
        try {
            Class.forName(Keys.MY_SQL_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(
                Keys.MY_SQL_DB_URL,
                Keys.MY_SQL_DB_USER,
                Keys.MY_SQL_DB_PASS);
    }
}
