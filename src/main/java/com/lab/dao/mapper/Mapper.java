package com.lab.dao.mapper;

public interface Mapper<T, D> {
    T map(D dataSet);
}
