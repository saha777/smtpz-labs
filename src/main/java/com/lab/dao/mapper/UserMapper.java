package com.lab.dao.mapper;

import com.lab.model.User;

public interface UserMapper<S> extends Mapper<User, S> {
    String ID_FIELD = "id";
    String NAME_FIELD = "name";
    String LASTNAME_FIELD = "lastname";
    String AGE_FIELD = "age";
}
