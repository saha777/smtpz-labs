package com.lab.servlets;

import com.lab.dao.DbType;
import com.lab.dao.UserDao;
import com.lab.dao.factory.UserDaoFactory;
import com.lab.dao.mapper.Mapper;
import com.lab.dao.mapper.UserMapper;
import com.lab.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "userServlet", urlPatterns = "/users/*")
public class UserServlet extends HttpServlet {
    private static String ALL_USERS_URL = "/users";
    private static String ALL_USERS_PAGE = "index.jsp";
    private static String ADD_USER_PAGE = "/WEB-INF/jsp/view/insert.jsp";
    private static String UPDATE_USER_PAGE = "/WEB-INF/jsp/view/update.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req, "allUsers");

        switch (action) {
            case "update":
                this.getUpdateUser(req, resp);
                break;
            case "insert":
                this.getInsertUser(req, resp);
                break;
            case "delete":
                this.delete(req, resp);
                break;
            case "allUsers":
            default:
                this.getAllUsers(req, resp);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req, "update");

        switch (action) {
            case "insert":
                postInsertUser(req, resp);
                break;
            default:
            case "update":
                postUpdateUser(req, resp);
                break;
        }
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int userId;

        try {
            userId = Integer.parseInt(req.getParameter("userId"));
        } catch (Exception e) {
            resp.sendRedirect(ALL_USERS_PAGE);
            return;
        }

        UserDao userDao = UserDaoFactory.create(DbType.MY_SQL);

        userDao.delete(userId);

        resp.sendRedirect(ALL_USERS_URL);
    }


    private void getAllUsers(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardAllUserTo(ALL_USERS_PAGE, UserDaoFactory.create(DbType.MY_SQL), req, resp);
    }

    private void getUpdateUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int userId;

        try {
            userId = Integer.parseInt(req.getParameter("userId"));
        } catch (Exception e) {
            resp.sendRedirect(ALL_USERS_PAGE);
            return;
        }

        UserDao userDao = UserDaoFactory.create(DbType.MY_SQL);

        User user = userDao.findById(userId);

        req.setAttribute("user", user);

        forwardAllUserTo(UPDATE_USER_PAGE, userDao, req, resp);
    }

    private void getInsertUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardAllUserTo(ADD_USER_PAGE, UserDaoFactory.create(DbType.MY_SQL), req, resp);
    }

    private void postUpdateUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDao userDao = UserDaoFactory.create(DbType.MY_SQL);

        User user;

        try {
            user = mapUser(req);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect(ALL_USERS_URL);
            return;
        }

        userDao.update(user);

        resp.sendRedirect(ALL_USERS_URL);
    }

    private void postInsertUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDao userDao = UserDaoFactory.create(DbType.MY_SQL);

        User user;

        try {
            user = mapUser(req);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect(ALL_USERS_URL);
            return;
        }

        userDao.insert(user);

        resp.sendRedirect(ALL_USERS_URL);
    }

    private void forwardAllUserTo(String path, UserDao userDao, HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        List<User> users = userDao.findAll();
        req.setAttribute("users", users);
        req.getRequestDispatcher(path).forward(req, resp);
    }

    private User mapUser(HttpServletRequest req) {
        Mapper<User, HttpServletRequest> userMapper = dataSet -> {
            User user = new User();

            String idStr = dataSet.getParameter(UserMapper.ID_FIELD);
            String name = dataSet.getParameter(UserMapper.NAME_FIELD);
            String lastname = dataSet.getParameter(UserMapper.LASTNAME_FIELD);
            byte age = Byte.parseByte(dataSet.getParameter(UserMapper.AGE_FIELD));

            if (idStr != null) {
                user.setId(Integer.parseInt(idStr));
            }
            user.setName(name);
            user.setLastname(lastname);
            user.setAge(age);
            return user;
        };
        return userMapper.map(req);
    }

    private String getAction(HttpServletRequest req, String defaultAction) {
        String pathInfo = req.getPathInfo(); // /{action}

        if (pathInfo == null) {
            return defaultAction;
        }

        String[] pathParts = pathInfo.split("/");

        if (pathParts.length < 2 || pathParts[1].isEmpty()) {
            return  "allUsers";
        }

        return pathParts[1];
    }
}
