<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Create User</h1>

<form action="/users/insert" method="post" name="user">
    <p>Name: </p>
    <input type="text" name="name" required>
    <p>Lastame: </p>
    <input type="text" name="lastname" required>
    <p>Age: </p>
    <input type="number" name="age" required>
    <input type="submit" title="Add">
</form>
<a href="/users">Back</a>