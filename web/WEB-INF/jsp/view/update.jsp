<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Update User</h1>

<form action="/users/update" method="post">
    <p>ID: </p>
    <input type="text" name="id" value="${user.id}" >
    <p>Name: </p>
    <input type="text" name="name" value="${user.name}">
    <p>Lastame: </p>
    <input type="text" name="lastname" value="${user.lastname}">
    <p>Age: </p>
    <input type="number" name="age" value="${user.age}">
    <input type="submit" title="Add">
</form>
<a href="/users">Back</a>