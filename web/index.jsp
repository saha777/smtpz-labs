<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Users</title>
</head>
<body>

<h1>Users</h1>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th colspan="2"><a href="/users/insert">Add</a></th>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td><a href="/users/update?userId=${user.id}">Update</a></td>
            <td><a data-method="delete" href="/users/delete?userId=${user.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>

<a href="/users">refresh</a>

</body>
</html>